FROM openjdk:8-jdk-alpine
COPY target/*.jar js-demo-0.0.1-SNAPSHOT.jar
#COPY src/main/resources/logback-spring.xml /deployments/
EXPOSE 5561
CMD ["java", "-jar", "js-demo-0.0.1-SNAPSHOT.jar"]
