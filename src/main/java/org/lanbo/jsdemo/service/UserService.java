package org.lanbo.jsdemo.service;

import org.lanbo.jsdemo.model.Role;
import org.lanbo.jsdemo.model.User;
import java.util.List;

/**
 * Interface that represents {@link org.springframework.stereotype.Service} methods
 * @author Serge Tyulnikov
 * @version 1.0
 */
public interface UserService {

    User findByUsername(String username);

    User getById(Long id);

    void save(User user);

    void delete(Long id);

    void update(User user);

    List<User> getAll();

    List<Role> getAllRoles();
}
