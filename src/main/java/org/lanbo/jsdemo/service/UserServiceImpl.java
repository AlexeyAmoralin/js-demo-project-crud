package org.lanbo.jsdemo.service;

import lombok.extern.slf4j.Slf4j;
import org.lanbo.jsdemo.model.Role;
import org.lanbo.jsdemo.model.User;
import org.lanbo.jsdemo.repository.RoleRepository;
import org.lanbo.jsdemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Service for {@link User} that implements {@link UserService}
 * @author Serge Tyulnikov
 * @version 1.0
 */

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User findByUsername(String username) {
        User result = userRepository.findByUsername(username);
        log.info("IN findByUsername - user: {} found by username: {}", result, username);
        return result;
    }

    @Override
    public User getById(Long id) {
        User result = userRepository.findById(id).orElse(null);
        if (result == null) {
            log.warn("IN getById - no user found by id: {}", id);
            return null;
        }

        log.info("IN getById - user: {} found by id: {}", result.getUsername(), id);
        return result;
    }

    @Override
    public void save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        log.info("IN save - user: {} with roles: {} successfully saved", user.getUsername(), user.rolesToString());
    }

    @Override
    public void update(User user) {
        String oldPassword = userRepository.findById(user.getId()).orElse(null).getPassword();
        if (!oldPassword.equals(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
        log.info("IN delete - user with id: {} successfully deleted", id);
    }

    @Override
    public List<User> getAll() {
        List<User> result = userRepository.findAll();
        log.info("IN getAll - {} users found", result.size());
        return result;
    }

    @Override
    public List<Role> getAllRoles() {
        return roleRepository.findAll();
    }
}
