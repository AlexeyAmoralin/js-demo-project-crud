package org.lanbo.jsdemo.repository;

import org.lanbo.jsdemo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for {@link User}
 * @author Serge Tyulnikov
 * @version 1.0
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
