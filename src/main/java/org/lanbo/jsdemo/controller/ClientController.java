package org.lanbo.jsdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Class that represents controller for views
 * @author Serge Tyulnikov
 * @version 1.0
 */

@Controller
public class ClientController {

    @GetMapping("/login")
    public String getLoginPage() {
        return "login";
    }

    @GetMapping("/admin")
    public String admin() {
        return "index";
    }

}
