package org.lanbo.jsdemo.rest;

import org.lanbo.jsdemo.dto.RoleDto;
import org.lanbo.jsdemo.dto.UserDto;
import org.lanbo.jsdemo.mappers.RoleMapper;
import org.lanbo.jsdemo.mappers.UserMapper;
import org.lanbo.jsdemo.model.Role;
import org.lanbo.jsdemo.model.User;
import org.lanbo.jsdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class that represents REST-controller
 * @author Serge Tyulnikov
 * @version 1.0
 */
@RestController
@RequestMapping("admin/users/")
public class AdminRestControllerV1 {

    @Autowired
    private UserService userService;

    @GetMapping(value = "{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable("id") Long customerId) {
        if (customerId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        User user = this.userService.getById(customerId);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(UserMapper.INSTANCE.toDTO(user), HttpStatus.OK);
    }

    @PostMapping(value = "")
    public ResponseEntity<User> saveUser(@RequestBody User user) {
        HttpHeaders headers = new HttpHeaders();

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.userService.save(user);
        return new ResponseEntity<>(user, headers, HttpStatus.CREATED);
    }


    @PutMapping(value = "")
    public ResponseEntity<User> updateUser(@RequestBody User user, UriComponentsBuilder builder) {
        HttpHeaders httpHeaders = new HttpHeaders();

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.userService.update(user);

        return new ResponseEntity<>(user, httpHeaders, HttpStatus.OK);
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<User> deleteUser(@PathVariable("id") Long id) {
        User user = this.userService.getById(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        this.userService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "")
    public ResponseEntity<List<UserDto>> getAllUsers() {
        List<User> users = this.userService.getAll();
        List<UserDto> userDtos = users.stream().map(UserMapper.INSTANCE::toDTO).collect(Collectors.toList());

        if (users.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(userDtos, HttpStatus.OK);
    }

    @GetMapping(value = "/roles/")
    public ResponseEntity<List<RoleDto>> getAllRoles() {
        List<Role> roles = this.userService.getAllRoles();
        List<RoleDto> roleDtos = roles.stream().map(RoleMapper.INSTANCE::toDTO).collect(Collectors.toList());

        if (roles.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(roleDtos, HttpStatus.OK);
    }

}
