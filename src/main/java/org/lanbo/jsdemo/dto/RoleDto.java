package org.lanbo.jsdemo.dto;

import lombok.Data;

/**
 * Class that represents {@link org.lanbo.jsdemo.model.Role} data transfer object
 * @author Serge Tyulnikov
 * @version 1.0
 */
@Data
public class RoleDto {
    private Long id;
    private String name;
}
