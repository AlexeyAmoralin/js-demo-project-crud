package org.lanbo.jsdemo.dto;


import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Class that represents {@link org.lanbo.jsdemo.model.User} data transfer object
 * @author Serge Tyulnikov
 * @version 1.0
 */
@Data
public class UserDto {

    private Long id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    private BigDecimal budget;
    private List<RoleDto> roles;
}
