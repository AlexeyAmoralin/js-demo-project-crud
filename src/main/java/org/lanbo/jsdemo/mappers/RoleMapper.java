package org.lanbo.jsdemo.mappers;

import org.lanbo.jsdemo.dto.RoleDto;
import org.lanbo.jsdemo.model.Role;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RoleMapper {
    RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);

    RoleDto toDTO(Role role);
}
