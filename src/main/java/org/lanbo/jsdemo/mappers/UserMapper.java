package org.lanbo.jsdemo.mappers;

import org.lanbo.jsdemo.dto.UserDto;
import org.lanbo.jsdemo.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Class that represents DTO-mapper for {@link User}
 * @author Serge Tyulnikov
 * @version 1.0
 */
@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto toDTO(User user);
}
