const requestUrl = 'http://localhost:8080/admin/users/'
const requestRoles = 'http://localhost:8080/admin/users/roles/'
const requestCurrent = 'http://localhost:8080/user/current/'
let rolesBuffer


/**
 * Function that sends a 'GET' request
 * @param url
 * @returns {Promise<Response | void>}
 */
function sendGetRequest(url) {
    const headers = {
        'Content-Type': 'application/json'
    }
    return fetch(url, {
        method: 'GET',
        headers: headers
    })
        .then(response => response.json())
        .catch(err => console.error(err))
}


/**
 * Function that sends a 'DELETE' request
 * @param url
 * @returns {Promise<void>}
 */
function sendDeleteRequest(url) {
    const headers = {
        'Content-Type': 'application/json',
    }
    return fetch(url, {
        method: 'DELETE',
        headers: headers
    })
        .then(response => console.log(response))
        .catch(err => console.error(err))
}


/**
 * Function that sends a POST/PUT request
 * @param url
 * @param method
 * @param body
 * @returns {Promise<Response>}
 */
function sendUpdateRequest(url, method, body = null) {
    const headers = {
        'Content-Type': 'application/json'
    }
    return fetch(url, {
        method: method,
        body: JSON.stringify(body),
        headers: headers
    }).then(response => {
        if (response.ok) {
            return response.json()
        }
        return response.json().then(error => {
            const e = new Error('Something wrong')
            e.data = error
            throw e
        })
    })
}


/**
 * Function that append data from JSON array to HTML users table
 * @param data
 */
function appendData(data) {
    let mainContainer = document.getElementById("tableData")
    for (let i = 0; i < data.length; i++) {
        let tableRow = document.createElement("tr")
        tableRow.id = data[i].id
        tableRow.innerHTML =
            "<td>" + data[i].id + "</td>" +
            "<td>" + data[i].username + "</td>" +
            "<td>" + data[i].firstName + "</td>" +
            "<td>" + data[i].lastName + "</td>" +
            "<td>" + data[i].address + "</td>" +
            "<td>" + data[i].budget + "</td>" +
            "<td>" + rolesToString(data[i].roles) + "</td>" +
            "<td><button class='btn btn-warning' data-toggle='modal' onclick='editModal()'>Edit</button></td>" +
            "<td><button class='btn btn-danger' data-toggle='modal' onclick='deleteModal()'>Delete</button></td>"
        mainContainer.appendChild(tableRow)
    }
}


/**
 * Launcher for our table of users
 */
function documentReady() {
    $(document).ready(() => {
        sendGetRequest(requestCurrent).then(data => {
            console.log(data)
            document.getElementById("currentUsername").innerText = data.username
            document.getElementById("currentRoles").innerText = rolesToString(data.roles)
            document.getElementById("userID").innerText = data.id
            document.getElementById("userUsername").innerText = data.username
            document.getElementById("userFirstName").innerText = data.firstName
            document.getElementById("userLastName").innerText = data.lastName
            document.getElementById("userAddress").innerText = data.address
            document.getElementById("userBudget").innerText = data.budget
            document.getElementById("userRoles").innerText = rolesToString(data.roles)
            data.roles.forEach(role => {
                if (role.name === "ROLE_ADMIN") {
                    adminRequests()
                }
            })

        })

    })
}


/**
 * Function that represents Admin's requests
 */
function adminRequests() {
    document.getElementById("addCheckbox").innerHTML = ''
    document.getElementById("editCheckbox").innerHTML = ''
    sendGetRequest(requestUrl).then(data => appendData(data))
    sendGetRequest(requestRoles).then(data => {
        rolesBuffer = data
        createCheckbox(data, "addCheckbox")
        createCheckbox(data, "editCheckbox")
    })
}


/**
 * Function to create checkboxes for roles
 * @param data
 * @param elementName
 */
function createCheckbox(data, elementName) {
    const divCheckbox = document.getElementById("" + elementName + "")
    for (let i = 0; i < data.length; i++) {
        let label = document.createElement("label")
        label.innerText = rolesBuffer[i].name
        let checkbox = document.createElement("input")
        checkbox.setAttribute("type", "checkbox")
        checkbox.setAttribute("id", rolesBuffer[i].id + elementName)
        divCheckbox.appendChild(label)
        divCheckbox.appendChild(checkbox)

        checkbox.addEventListener('change', () => {
            if (checkbox.hasAttribute("checked")) {
                checkbox.removeAttribute("checked")
            } else {
                checkbox.setAttribute("checked", "true")
            }

        })
    }
}


/**
 * Secondary function toString() for array of roles from JSON
 * @param roles
 * @returns {string}
 */
function rolesToString(roles) {
    let result = '';
    for (let i = 0; i < roles.length; i++) {
        result += roles[i].name + " "
    }
    return result;
}


/**
 * Function that invoke addModalWindow
 */
function addModal() {

    clearCheckboxes("addCheckbox")

    $("#addModal").modal('show')
}


/**
 * Function that add a new user to our table
 */
function addUser() {
    console.log(document.getElementById(1 + "addCheckbox").hasAttribute("checked"))
    console.log(document.getElementById(2 + "addCheckbox").hasAttribute("checked"))
    let rolesArray = []
    rolesBuffer.forEach(role => {
        console.log("Entry point!")
        if (document.getElementById(role.id + "addCheckbox")
            .hasAttribute("checked")) {
            console.log("CHECKED")
            rolesArray.push({
                id: role.id,
                name: role.name
            })
        }
        console.log(role.id + "addCheckbox")
    })

    console.log(rolesArray)

    const body = {
        username: $("#addUsername").val(),
        password: $("#addPassword").val(),
        firstName: $("#addFirstname").val(),
        lastName: $("#addLastname").val(),
        address: $("#addAddress").val(),
        budget: $("#addBudget").val(),
        roles: rolesArray
    }

    sendUpdateRequest(requestUrl, 'POST', body).then(data => {
        console.log(data)
        document.getElementById("tableData").innerHTML = ''
        documentReady()
    })
        .then($("#addModal").modal('hide'))
        .then(formClear)
}


/**
 * Function that invoke editModalWindow
 */
function editModal() {

    clearCheckboxes("editCheckbox")

    $("#editModal").modal('show')


    let id = event.target.parentNode.parentNode.id

    sendGetRequest(requestUrl + id)
        .then(data => {
            console.log(data)
            return data
        })
        .then(data => {
            $("#editID").val(data.id)
            $("#editUsername").val(data.username)
            $("#editPassword").val(data.password)
            $("#editFirstname").val(data.firstName)
            $("#editLastname").val(data.lastName)
            $("#editAddress").val(data.address)
            $("#editBudget").val(data.budget)

            rolesBuffer.forEach(roleFromBuffer => {
                data.roles.forEach(role => {
                    if (role.name === roleFromBuffer.name) {
                        document.getElementById(roleFromBuffer.id + "editCheckbox")
                            .setAttribute("checked", "true")
                    }
                })
            })


        })

}

/**
 * Function that edit user
 */
function editUser() {

    let rolesArray = []
    rolesBuffer.forEach(role => {
        if (document.getElementById(role.id + "editCheckbox")
            .hasAttribute("checked")) {
            rolesArray.push({
                id: role.id,
                name: role.name
            })
        }
    })

    const body = {
        id: $("#editID").val(),
        username: $("#editUsername").val(),
        password: $("#editPassword").val(),
        firstName: $("#editFirstname").val(),
        lastName: $("#editLastname").val(),
        address: $("#editAddress").val(),
        budget: $("#editBudget").val(),
        roles: rolesArray
    }

    sendUpdateRequest(requestUrl, 'PUT', body).then(data => {
        console.log(data)
        document.getElementById("tableData").innerHTML = ''
        documentReady()
    })
        .then($("#editModal").modal('hide'))

}

/**
 * Function that invoke deleteModalWindow
 */
function deleteModal() {

    let id = event.target.parentNode.parentNode.id

    $("#deleteModal").modal("show")
    sendGetRequest(requestUrl + id)
        .then(data => {
            console.log(data)
            return data
        })
        .then(data => {
            $("#deleteID").val(data.id)
            $("#deleteUsername").val(data.firstName)
            $("#deletePassword").val(data.firstName)
            $("#deleteFirstname").val(data.firstName)
            $("#deleteLastname").val(data.lastName)
            $("#deleteAddress").val(data.address)
            $("#deleteBudget").val(data.budget)
        })


}

/**
 * Function that delete user
 */
function deleteUser() {
    sendDeleteRequest(requestUrl + $("#deleteID").val())
        .then(data => {
            console.log(data)
            document.getElementById("tableData").innerHTML = ''
            documentReady()
        })
        .then($("#deleteModal").modal('hide'))
}


/**
 * Function that clears checkboxes
 * @param elementName
 */
function clearCheckboxes(elementName) {
    rolesBuffer.forEach(role => {
        document.getElementById(role.id + elementName)
            .removeAttribute("checked")
    })
}


/**
 * Function for clear addModalWindow after adding a new entity
 */
function formClear() {
    $("#addUsername").val("");
    $("#addPassword").val("");
    $("#addFirstname").val("");
    $("#addLastname").val("");
    $("#addAddress").val("");
    $("#addBudget").val("");
}


/**
 * The procedure that invoke launcher of table
 */
documentReady()














